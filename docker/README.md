# iqrf-gateway-daemon-image

Docker image for IQRF Gateway.

```bash
docker build -f amd64-latest.Dockerfile -t iqrftech/iqrf-gateway-daemon:latest-amd64 .
docker push iqrftech/iqrf-gateway-daemon:latest-amd64
```