Support
=======

IQRF Gateway team strongly believes in open source software and collaboration.

Public repositories
-------------------

* Source code

  * https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp

Public development
------------------

* Tracking

  * https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/milestones
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/milestones

* Improvements 

  * https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/merge_requests
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/merge_requests

* Documentation 
  
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/docs
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/docs
  * https://docs.iqrf.org/iqrf-gateway-daemon
  * https://docs.iqrf.org/iqrf-gateway-webapp

Report bugs
-----------

* Create new issue
  
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/issues
  * https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/issues

* Describe the issue and attach logs from the gateway
* Logs can be downloaded from the gateway via http://IQRF-Gateway-Webapp-IP/gateway/info/ - Download diagnostics  

We believe that with your active participation the IQRF Gateway can be greatly improved.

Enjoy!

IQRF Gateway Team

https://gitlab.iqrf.org/open-source
